#ifndef _NWAVE_TYPEDEF_H_
#define _NWAVE_TYPEDEF_H_

#include <stdint.h>
#include <stdbool.h>

typedef uint8_t U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;

typedef int8_t S8;
typedef int16_t S16;
typedef int32_t S32;
typedef int64_t S64;

typedef unsigned int UI;

#define BIT0 (1ul << 0)
#define BIT1 (1ul << 1)
#define BIT2 (1ul << 2)
#define BIT3 (1ul << 3)
#define BIT4 (1ul << 4)
#define BIT5 (1ul << 5)
#define BIT6 (1ul << 6)
#define BIT7 (1ul << 7)
#define BIT8 (1ul << 8)
#define BIT9 (1ul << 9)
#define BIT10 (1ul << 10)
#define BIT11 (1ul << 11)
#define BIT12 (1ul << 12)
#define BIT13 (1ul << 13)
#define BIT14 (1ul << 14)
#define BIT15 (1ul << 15)

#define BIT_MASK(num_of_bit) ((1ul << (num_of_bit)) - 1)

typedef enum
{
	eExtOff = 1,
	eExtOn = 2
}eExtOnOff;


#endif
