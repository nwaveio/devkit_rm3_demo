#include "main.h"

#ifndef EXAMPLE_CODE 
#error "User example code is not defined"
#endif

unsigned char send_buffer[20];
unsigned char send_buffer_len;
int g_command_length = 0;

/* Counts 1ms timeTicks */
volatile uint32_t msTicks;
/* Counts 1ms timeTicks */
uint32_t sTicks;

//void Delay(uint32_t dlyTicks);

uint32_t get_ms_counter(void) {
	return msTicks;
}

void delay(uint32_t dlyTicks);
static void all_init(void);
void init_printf(void *putp, void (*putf)(void *, char), void (*start)(void *),
		void (*stop)(void *));
void tfp_printf(char *fmt, ...);

static bool button_pressed = false;
static unsigned long lastDetectedPressTime = 0;

#ifdef SEND_MSG_BTN
static unsigned char iterator_test_text[8] = { 0xCA, 0xFE, 0xBE, 0xEF, 0xFE,
		0xED, 0xBA, 0xBE };
#endif

void SysTick_Handler(void) {
	msTicks++; /* increment counter necessary in Delay()*/
	sTicks++;
}

void GPIO_ODD_IRQHandler(void) {
	GPIO_IntClear(GPIO_IntGet());
	if (msTicks - lastDetectedPressTime > 100) {
		GPIO_IntConfig(gpioPortB, 13, false, true, false);
		button_pressed = true;
		lastDetectedPressTime = msTicks;
	}
}

void delay(uint32_t dlyTicks) {
	uint32_t curTicks;

	curTicks = sTicks;
	while ((sTicks - curTicks) < dlyTicks)
		;
}

#if EXAMPLE_CODE==UART_2_RM
void uart_2_rm (void)
{
	int watch_c;
	while ((watch_c = NWRM_UART_GetChar()) >= 0) {
		send_buffer[g_command_length++] = watch_c;
		if (g_command_length >= 4) {
			//unsigned char packetRec[256];
			//#warning "Set a proper frequency value for your environment"
			/* NWAVE_Set_Frequency(866500000, 50000, 100); */
			/* NWAVE_Set_Frequency(868800000, 50000, 100); */
			/* NWAVE_Set_Frequency(916500000, 50000, 100); */
			NWAVE_send(send_buffer, 4 /*send_buffer_len*/, NULL, PROTOCOL_B);

			tfp_printf("Data sent.\n");
			g_command_length = 0;
		}
	}
}
#elif EXAMPLE_CODE==AT_PARSER

extern void at_command_parser(void);

#endif

#define LED_PORT gpioPortC
#define LED_PIN 15

static void LedInit(void) {

	//CMU_ClockEnable(cmuClock_HFPER, true);
	//CMU_ClockEnable(cmuClock_GPIO, true);

	GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 0);
}

static void LedSetState(bool state) {
	if (state) {
		GPIO_PinOutSet(LED_PORT, LED_PIN);
	} else {
		GPIO_PinOutClear(LED_PORT, LED_PIN);
	}
}

static void LedToggle(void) {
	static bool blink;
	blink = !blink;
	LedSetState(blink);
}

static void ButtonInit(void) {
	// Define the button pin as an input with an internal pull-up resistor

	GPIO_PinModeSet(gpioPortB, 13, gpioModeInputPull, 1);
	// Enable falling edge interrupts on button pin

	GPIO_IntConfig(gpioPortB, 13, false, true, true);

	NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);

	NVIC_EnableIRQ(GPIO_ODD_IRQn);
}

static void all_init(void) {
	//-----------General initialization
	/* Chip errata */
	CHIP_Init();

	CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_32);

	/* Setup SysTick Timer for 1 msec interrupts  */
	if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000))
		while (1)
			;

	CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_1);
	if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000))
		while (1)
			;

	CMU_ClockEnable(cmuClock_GPIO, true);

	// Initialize the clock Management Unit
	NWRM_CMU_Init(true);

	// Initialize the RTC clock
	NWRM_RTC_Init(0);

	//----------NWave specific initialization
	NWAVE_SetMsGetterFunc(get_ms_counter, delay);
#warning "Set a proper frequency value for your environment"
	NWAVE_Set_Frequency(866500000, 50000, 100);
	/* NWAVE_Set_Frequency(868800000, 50000, 100); */
	/* NWAVE_Set_Frequency(916500000, 50000, 100); */

	delay(200);

	//--------User specific initialization
	GPIO_PinModeSet(gpioPortC, 1, gpioModeInput, 0);
	GPIO_PinModeSet(gpioPortC, 0, gpioModePushPull, 0);

	LedInit();
	ButtonInit();

	init_printf(NWRM_UART_Init(9600, true, false), NWRM_UART_Putc,
			NWRM_UART_Start, NWRM_UART_Stop);

}

void user_setup(void) {
	all_init();

#if EXAMPLE_CODE==UART_2_RM        
	tfp_printf("This is UART TO NWAVE example, each 4 bytes coming to UART are sent to the radio module transmitter.\n" );
#elif EXAMPLE_CODE==AT_PARSER    
	tfp_printf("Welcome to Nwave console!\nInput an AT command:\n");
#endif    
}

void user_loop(void) {
	if (button_pressed == true) {
#ifdef SEND_MSG_BTN    
		button_pressed = false;
		NWAVE_send(iterator_test_text, 8, NULL, PROTOCOL_B);
		iterator_test_text[7]++;
#endif    
		GPIO_IntConfig(gpioPortB, 13, false, true, true);
	}
#ifdef LED_BLINKING    
	LedToggle();//when you debug (jtag,swd) MCU doesn't sleep so blinking is too fast to see
#endif        
#if EXAMPLE_CODE==UART_2_RM
	uart_2_rm();
#elif EXAMPLE_CODE==AT_PARSER
	at_command_parser();
#endif     
}

int main(void) {
	user_setup();

	while (1) {
		RTC_CounterReset();
		NWRM_RTC_Init(0);
		NWRM_RTC_Sleep();

		user_loop();

	}
}

