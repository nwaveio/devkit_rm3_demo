#ifndef _CIRCULAR_BUFFER_H_
#define _CIRCULAR_BUFFER_H_
#include "nwave_typedef.h"

typedef struct
{
    uint8_t * const buffer;
    int head;
    int tail;
    const int maxLen;
}CircularBuffer;

int CircBuf_Push(CircularBuffer *c, uint8_t data);
int CircBuf_Pop(CircularBuffer *c, uint8_t *data);
UI CircBuf_DataCount(const CircularBuffer *c);

#define CIRCBUF_DEF(x,y) uint8_t x##_space[y]; CircularBuffer x = { x##_space,0,0,y}

#endif
