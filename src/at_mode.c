#include "main.h"
#include "nwrm_flash.h"



#if EXAMPLE_CODE==AT_PARSER



extern unsigned long FreqLow;
extern unsigned long FreqHigh;

static unsigned int freq;
static unsigned int bandw;



extern int g_command_length;

extern unsigned char send_buffer[20];
extern unsigned char send_buffer_len;


static char at_cmd_buffer[AT_MAX_LEN];



//s - null terminated string
static bool compare(const char * data, int data_length, const char * s) {
	while (*s) {
		if (*s++ != *data++) {
			return false;
		}
		data_length--;
	}
	return data_length >= 0;
}

static void at_frequency_command(void) {
	if (at_cmd_buffer[8] == '\n') {
		tfp_printf("Error\n");
		return;
	} else if (at_cmd_buffer[8] == '?') {

		tfp_printf("FreqLow:%d FreqHigh:%d\n", FreqLow, FreqHigh);
		//tfp_printf("freq:%d bandw:%d\n",freq, bandw);
		return;
	}

	unsigned char temp1 = 0;
	unsigned char temp2 = 0;
	while (at_cmd_buffer[temp1] != ',') {
		if (at_cmd_buffer[temp1] == '\n') {
			break;
		}
		temp1++;
	}
	if (at_cmd_buffer[temp1] == '\n') {
		tfp_printf("Error\n");
		return;
	}
	freq = atoi((char *) (at_cmd_buffer + sizeof("AT+FREQ=") - 1));

	if ((freq < RF_RANGE_LOW_END) || (freq > RF_RANGE_HIGH_END)) {
		tfp_printf("Error\n");
		return;
	}

	while (at_cmd_buffer[temp1 + temp2] != '\n')
		temp2++;
	{
		bandw = atoi((char *) (at_cmd_buffer + temp1 + 1));
		if (bandw == 0) { // Zero is a typical error code after attempt to convert letters
			tfp_printf("Error\n");
		} else if (((freq - bandw / 2) < RF_RANGE_LOW_END)
				|| ((freq + bandw / 2) > RF_RANGE_HIGH_END)) {
			tfp_printf("Error\n");
		} else {
			NWAVE_Set_Frequency(freq, bandw, 100);
			tfp_printf("Ok\n");
		}
	}
}



static void at_send_and_receive_command(void) {
	unsigned char temp1 = 0;
	temp1 = sizeof("AT+RCV=") - 1;
	unsigned char j = 0;
	while (at_cmd_buffer[temp1] != '\n') {
		if (at_cmd_buffer[temp1 + 1] == '\n')
			break;
		if (at_cmd_buffer[temp1] == '$') {
			unsigned char arr[3] = { 0, 0, '\n' };
			temp1++;
			arr[0] = at_cmd_buffer[temp1++];
			arr[1] = at_cmd_buffer[temp1++];
			int dat;
			sscanf((char *) arr, "%x", &dat);
			send_buffer[j++] = dat;
			send_buffer_len = j;
			if (j > 20) {
				tfp_printf("Error\n");
			}
		} else {
			tfp_printf("Error\n");
		}
	}
	if (send_buffer_len == 0) {
		tfp_printf("Error\n");
	} else {
		char packetRec[256];
		unsigned char res = 0;
		res = NWAVE_send(send_buffer, send_buffer_len, (unsigned char*)packetRec,
				PROTOCOL_E);
		//tfp_printf("Res:%d\n", res);
		if (res > 0) {
			NWRM_UART_Send(packetRec, res);
		} else {
			init_printf(NWRM_UART_Init(9600, true, false), NWRM_UART_Putc,
					NWRM_UART_Start, NWRM_UART_Stop);
			tfp_printf("Error\n");
		}
	}
}

static void at_send_command(void) {
	unsigned char temp1 = 0;
	bool send_data_valid = true; //and later maybe changed to false if input data is wrong
	temp1 = sizeof("AT+SEND=") - 1;
	unsigned char j = 0;
	while (at_cmd_buffer[temp1] != '\n') {
		if (at_cmd_buffer[temp1 + 1] == '\n')
			break;
		if (at_cmd_buffer[temp1] == '$') {
			unsigned char arr[3] = { 0, 0, '\n' };
			temp1++;
			arr[0] = at_cmd_buffer[temp1++];
			arr[1] = at_cmd_buffer[temp1++];
			int dat;
			sscanf((char *) arr, "%x", &dat);
			send_buffer[j++] = dat;
			send_buffer_len = j;
			if (j > 20) {
				tfp_printf("Error\n");
			}
		} else {
			tfp_printf("Error\n");
			send_data_valid = false;
			break;
		}
	}
	if (send_buffer_len == 0) {
		tfp_printf("Error\n");
	} else if (send_data_valid == true) {
		//unsigned char packetRec[256];
		NWAVE_send(send_buffer, send_buffer_len, NULL,
								PROTOCOL_B);
				tfp_printf("Ok\n");
	}
}

static void at_serial_command(void) {
	unsigned char temp1 = 0;
	switch (at_cmd_buffer[10]) {
	case '?': {
		NWRM_DEVICE device;
		NWRM_FLASH_DeviceRead(&device);
		unsigned char buffer[8];
		sprintf((char *) buffer, "%08x", device.Serial);
		tfp_printf((char *) buffer);
		tfp_printf("\n");
	}
		break;
	case '$': {
		bool serial_valid = true; //and later maybe changed to false if input data is wrong
		temp1 = sizeof("AT+SERIAL=") - 1;
		unsigned char j = 0;
		send_buffer_len = 0;
		while (at_cmd_buffer[temp1] != '\n') {
			if (at_cmd_buffer[temp1 + 1] == '\n')
				break;
			if (at_cmd_buffer[temp1] == '$') {
				unsigned char arr[3] = { 0, 0, '\n' };
				temp1++;
				arr[0] = at_cmd_buffer[temp1++];
				arr[1] = at_cmd_buffer[temp1++];
				int dat;
				sscanf((char *) arr, "%x", &dat);
				send_buffer[j++] = dat;
				send_buffer_len = j;
				if (j > 8) {
					tfp_printf("Error\n");
				}
			} else {
				tfp_printf("Error\n");
				serial_valid = false;
				break;
			}
		}
		if (serial_valid) {
			NWRM_DEVICE device;
			device.Serial = 0;
			for (unsigned char z = 0; z < send_buffer_len; z++) {
				if (z > 0)
					device.Serial <<= 8;
				device.Serial |= *(unsigned char *) (send_buffer
						+ z);

			}
			NWRM_FLASH_DeviceWrite(&device);
			NWRM_FLASH_DeviceRead(&device);
			unsigned char buffer[8];
			sprintf((char *) buffer, "%08x", device.Serial);
			tfp_printf((char *) buffer);
			tfp_printf("\n");
		}

	}
		break;
	default:
		tfp_printf("Error\n");
	}

}

void at_command_parser(void) {
	int watch_c;

	while ((watch_c = NWRM_UART_GetChar()) >= 0) {
		if (watch_c >= 'a' && watch_c <= 'z') {
			// set all letters to upper case
			watch_c -= 'a' - 'A';
		}
		at_cmd_buffer[g_command_length++] = watch_c;

		if (g_command_length >= AT_MAX_LEN) {
			tfp_printf("Error\n");
			g_command_length = 0;
			watch_c = -1;
			break;
		}
		if (watch_c != 0xA)
			break;

		if (compare(at_cmd_buffer, g_command_length, "AT+FREQ=")) {
			at_frequency_command();
		} else if (compare(at_cmd_buffer, g_command_length, "AT+RCV=")) {
			at_send_and_receive_command();
		} else if (compare(at_cmd_buffer, g_command_length, "AT+SEND=")) {
			at_send_command();
		} else if (compare(at_cmd_buffer, g_command_length, "AT+SERIAL=")) {
			at_serial_command();
		} else if (compare(at_cmd_buffer, g_command_length, "ATB")) {
			tfp_printf("MCU is going to Reset...\n");
			/* Write to the Application Interrupt/Reset Command Register to reset
			 * the EFM32. See section 9.3.7 in the reference manual. */
			SCB ->AIRCR = 0x05FA0004;
		} else if (compare(at_cmd_buffer, g_command_length, "ATZ")) {
			tfp_printf("ATZ is not supported, use AT+FREQ=\n");
		} else {
			tfp_printf("Error\n");
		}
		g_command_length = 0;
		break;

	}
}


#endif

