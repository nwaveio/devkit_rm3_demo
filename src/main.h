#ifndef __MAIN_H
#define __MAIN_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "string.h"
#include "time.h"
#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_chip.h"
#include "unb.h"
#include "nwrm_uart.h"
#include "nwrm_cmu.h"
#include "nwrm_rtc.h"
#include "nwrm_printf.h"
#include "nwrm_flash.h"
#include "em_usart.h"
#include <em_leuart.h>

#define AT_MAX_LEN   256
#define RF_RANGE_LOW_END   858500000
#define RF_RANGE_HIGH_END   925500000

#define AT_PARSER  0
#define UART_2_RM    1

#define EXAMPLE_CODE  AT_PARSER



#define LED_BLINKING
#define SEND_MSG_BTN

#endif
