#ifndef UNB_H_
#define UNB_H_

#include "stdint.h"
enum 
{
	PROTOCOL_B = 0,
	PROTOCOL_E = 1
};

extern uint8_t RSSI,curRSSI,SNR;


typedef uint32_t (*get_ms_counter_t)(void);
typedef void (*ms_delay_t)(uint32_t);


void NWAVE_SetMsGetterFunc(get_ms_counter_t  get_ms, ms_delay_t delay_function);
unsigned char NWAVE_send(unsigned char *packet, unsigned char length, unsigned char* packetRec, unsigned char protocol);
int NWAVE_Set_Frequency(unsigned long carrying_frequency, unsigned long bandwidth, int channels);

extern ms_delay_t  DelayMs;

#define LORA_FREQUENCY 869200000.0//870000000.0

#define LORA_ADDRESS_OFFSET (0)

#define LORA_ADDRESS_LENGTH (3)



typedef enum
{

	PWR_MODE_ALWAYS_MAX = 0,

	PWR_MODE_LOW_IF_GOOD_LINK = 1

}pwr_mode_t;


/*
typedef struct

{

	uint32_t ID;					// device ID

	uint8_t retries;				// number of retries in case of no ack

	uint32_t no_ack_retry_period;	// period between retries in seconds (for protocol e)

	pwr_mode_t pwr_mode;			// adaptive power

	unsigned long freq_start;		// start of freqency band

	unsigned long freq_end;			// end of frequency band

	int number_of_channels;			// number of channels the band is divided in

}UNB_settings_t;



typedef struct

{

	uint32_t address:24;

	uint32_t type_ID:8;

	uint8_t payload[20];

}UNB_ack_packet_t;



uint8_t UNB_Init (UNB_settings_t* init);

uint8_t UNB_SendPacket (uint8_t protocol, uint8_t *tx_packet, uint8_t tx_length);

void UNB_AckCallback(uint8_t status);

uint8_t UNB_RetreiveRxPackets(uint8_t *rx_packet, uint8_t rx_length);
*/
#endif /* UNB_H_ */
