#include "circular_buffer.h"

int CircBuf_Push(CircularBuffer *c, uint8_t data)
{
    int next = c->head + 1;
    if (next >= c->maxLen)
        next = 0;

    // Cicular buffer is full
    if (next == c->tail)
        return -1;  // quit with an error

    c->buffer[c->head] = data;
    c->head = next;
    return 0;
}

int CircBuf_Pop(CircularBuffer *c, uint8_t *data)
{
    // if the head isn't ahead of the tail, we don't have any characters
    if (c->head == c->tail)
        return -1;  // quit with an error

    *data = c->buffer[c->tail];
    c->buffer[c->tail] = 0;  // clear the data (optional)

    int next = c->tail + 1;
    if(next >= c->maxLen)
        next = 0;

    c->tail = next;

    return 0;
}

UI CircBuf_DataCount(const CircularBuffer *c)
{
	if ( c->head >= c->tail)
		return (c->head - c->tail);
	else
		return (c->maxLen + c->head - c->tail);
}


